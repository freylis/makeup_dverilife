$(document).ready(function() {    
    
	if($("aside").length==0) {
		el = $(".b-content.row-fluid");
		$(el).addClass('wide-block');
	};

	$('#slider').tinycarousel({ duration: 500 });				 					 		

	$('select').selectbox();

	$('input[type="number"]').stepper({
		width: '60px'
	});

	$('#size-select').on('change', function(){
		var model = $('#model-name').val();
		var size = $(this).val();
		var item = $('#model-name').data('item');
		$.ajax({
			url: '/catalog/get_size/',
			data: {
				size: size,
				model: model,
				item: item
			},
			dataType: 'json',
			success: function(data){
				if (data.canvas){
					$('#canvas-price').html(data.canvas);
				};
				if (data.box){
					$('#box-price').html(data.box);
				};
				if (data.jamb){
					$('#jamb-price').html(data.jamb);
				};
				if (data.price){
					$('#final-price').html(data.price);
					$('#item-price').val(data.price);
				}

			},
			error: function(){

			}
		})
	});

	// смена изображение в карточке товара
	$('div.color-item img').on('click', function(){
		var _this = $(this);
		var cid = _this.data('colorid');
		// покажим нужный рисунок
		$('a.item-image').removeClass('active');
		var img = $('a.item-image[data-color="color__' + cid + '"]');
		img.addClass('active');
		var color_name = img.data('colorname');
		$('#colorname').text(color_name);
		// подсветим нужную превьюшку
		$('div.color-item img').removeClass('active');
		_this.addClass('active');
		$('#color-value').val(cid);

	});

	function hide_added_cart(){
		$('#add-to-cart').fadeOut(1500);
	}

	// add item to cart
	$('.buy-btn').on('click', function(){
		var item = $(this).data('iid');
		var data = $('#order-form').serialize();
		$.ajax({
			url: '/cart/add/',
			data: data,
			dataType: 'json',
			success: function(data){
				$('#add-to-cart').html(data.result);
				$('#add-to-cart').fadeIn(300);
				get_cart();
				setTimeout(hide_added_cart,5000);
			},
			error: function(){
				
			}
		})
		return false;
	});

	$('.fancybox').fancybox({
        beforeLoad: function() {
            this.title = $(this.element).attr('title');
        }
    });

    function get_cart(){
    	$.ajax({
    		url: '/cart/get/',
    		dataType: 'json',
    		success: function(data){
		    	$('#main-cart').html(data.result);
    		},
    		error: function(){

    		}
    	});
    };
    get_cart();

    // изменяем кол-во товаров в корзине
    $('.change-item-count').on('change', function(){
    	var _this = $(this);
    	var key = _this.data('key');
    	var val = _this.val();
    	$.ajax({
    		url: '/cart/changecount/',
    		data: {
    			key: key,
    			val: val
    		},
    		dataType: 'json',
    		success: function(data){
				$('.item-price[data-key="' + key + '"]')
					.html(data.new_price);
				$('#total-count').html(data.total_count);
				$('#total-price').html(data.total_price);
    		},
    		error: function(){}
    	});
    });

    // удаление изделий из корзины
    $('.remove-item').on('click', function(){
    	var _this = $(this);
    	var key = _this.data('key');
    	$.ajax({
    		url: '/cart/removeitem/',
    		data: {
    			key: key
    		},
    		success: function(data){
    			if (data.status){
    				_this.parent().parent().remove();
					$('#total-count').html(data.total_count);
					$('#total-price').html(data.total_price);
    			}
    		},
    		error: function(){}
    	});
    });
                   	    
});